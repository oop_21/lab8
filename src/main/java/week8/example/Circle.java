package week8.example;

public class Circle {
    private String name;
    private int redius;

    public Circle(String name, int radius){
        this.name = name;
        this.redius = radius;

    }
    public String getNameCir(){
        return name;
    }
    public int getRadiusCir(){
        return redius;
    }
    void cirArea(){
        System.out.println((name) + (" Area = ") + (Math.PI * (redius*redius)));
    }
    void cirPerimeter(){
        System.out.println((name) + (" Perimeter = ") + (2 * Math.PI*redius));
    }
    void cirInfo(){
        System.out.println("Name:" + name + " Radius:" + redius);
    }
}
