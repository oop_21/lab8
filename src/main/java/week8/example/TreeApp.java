package week8.example;

public class TreeApp {
    public static void main(String[] args) {
        Tree Tree1 = new Tree("Tree1",'1',5,10);
        Tree1.print();

        Tree Tree2 = new Tree("Tree1",'2',5,11);
        Tree2.print();
        for(int y = Tree.MIN_Y; y <= Tree.MAX_Y; y++){
            for(int x = Tree.MIN_X; x <= Tree.MAX_X; x++){
                if(Tree1.getX() == x && Tree1.getY() == y){
                    System.out.print(Tree1.getSymbol());
                } else if (Tree2.getX() == x && Tree2.getY() == y ){
                    System.out.print(Tree2.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
                System.out.println();
            }
    }
}
