package week8.example;

public class TestShape {
    public static void main(String[] args) {
        Rectangle();
        Circle();
        Triang();
    }
    public static void Rectangle(){
        Rectangle rec1 = new Rectangle("rec1",10,5);
        rec1.recInfo();
        rec1.recArea();
        rec1.recPerimeter();
        System.out.println();
        Rectangle rec2 = new Rectangle("rec2", 5, 3);
        rec2.recInfo();
        rec2.recArea();
        rec2.recPerimeter();
        System.out.println();
    }
    public static void Circle(){
        Circle circle1 = new Circle("Circle1",10);
        circle1.cirInfo();
        circle1.cirArea();
        circle1.cirPerimeter();
        System.out.println();
        Circle circle2 = new Circle("circle2",2);
        circle2.cirInfo();
        circle2.cirArea();
        circle2.cirPerimeter();
        System.out.println();
    }
    public static void Triang(){
        Triangle Triangle = new Triangle("tringle",5,5,6);
        Triangle.triInfo();
        Triangle.triArea();
        Triangle.triPerimeter();
    }
}
