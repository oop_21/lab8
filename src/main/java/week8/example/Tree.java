package week8.example;

public class Tree {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0 ;
    public static final int MIN_Y = 0 ;
    public static final int MAX_X = 19 ;
    public static final int MAX_Y = 19 ;

    public Tree(String name ,char symbol,int x , int y){
        this.name =name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public Tree(String name ,char symbol){
        this(name,symbol,0,0);
    }

        public void print() {
    System.out.println("tree: " + name + " X:" + x + " Y:" + y);
    }
        public void setName(String name){
            this.name = name;
    }
        public String getName(){
            return name;
    }
        public char getSymbol(){
            return symbol;
    }
        public int getX(){
            return x;
    }
        public int getY(){
            return y;
}

}
